# FreeDOS Help (AMB)

FreeDOS help files in AMB format


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## AMBHELP.LSM

<table>
<tr><td>title</td><td>FreeDOS Help (AMB)</td></tr>
<tr><td>version</td><td>0.1c</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-03-06</td></tr>
<tr><td>description</td><td>FreeDOS help files in AMB format</td></tr>
<tr><td>keywords</td><td>freedos, help, doc, text, amb</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU Free Documentation License, Version 1.1](LICENSE)</td></tr>
</table>
